<?php

/**
 * @file
 * Theme function and preprocess for text_formats_report_field_stats.
 */

/**
 * Preprocess function for theme_text_formats_report_field_stats().
 */
function template_preprocess_text_formats_report_field_stats(&$variables) {
  $entities = array();
  $bundles = array();
  $records = $variables['stats'];

  // Gather some more insight from the data.
  foreach ($records as $record) {
    $record->{t('Field name')} = $variables['field_name'];
    $entities[] = $record;
    $bundles[] = $record->bundle;
  }

  $variables['bundles_count'] = array_count_values($bundles);
  $variables['total_bundles'] = count($variables['bundles_count']);
  $variables['total_entities'] = array_sum($variables['bundles_count']);
  $variables['entities'] = $entities;
}

/**
 * Theme implementation.
 *
 * @see template_preprocess_text_formats_report_field_stats()
 */
function theme_text_formats_report_field_stats(&$variables) {
  if (count($variables['entities']) < 1) {
    return '<p>' . t("No data found.") . '</p>';
  }

  $content = '<h2>' . t("Entity bundles using the field <em>@fieldname</em> (@count)", array('@fieldname' => $variables['field_name'], '@count' => $variables['total_bundles'])) . '</h2>';
  $content .= '<p>' . t("Using the text format: <em>@formats</em>", array('@formats' => implode(', ', $variables['filter_formats']))) . '</p>';
  $content .= theme('table', text_formats_report_prepare_simple_table($variables['bundles_count'], array('Bundle name', 'Entities')));

  $content .= '<h2>' . t("Content for <em>@fieldname</em> (@count)", array('@fieldname' => $variables['field_name'], '@count' => $variables['total_entities'])) . '</h2>';
  $content .= '<p>' . t("Using the text format: <em>@formats</em>", array('@formats' => implode(', ', $variables['filter_formats']))) . '</p>';
  $content .= theme('table', text_formats_report_prepare_table($variables['entities'], array(
    t('Entity ID'),
    t('Entity type'),
    t('Bundle'),
    t('Text format'),
    t('Content'),
    t('Field name'),
  )));

  return $content;
}
