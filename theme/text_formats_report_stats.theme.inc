<?php

/**
 * @file
 * Theme function and preprocess for text_formats_report_stats.
 */

/**
 * Preprocess function for theme_text_formats_report_stats().
 */
function template_preprocess_text_formats_report_stats(&$variables) {
  // Prepare just the filter titles per filter format.
  $filter_lists = $variables['filter_formats_filter_lists'];
  $filter_lists_titles = array();
  foreach ($filter_lists as $filter_format => $row) {
    foreach ($row as $filter) {
      if ($filter->status == 1) {
        $filter_lists_titles[] = check_plain($filter->title);
      }
    }
    $variables['filter_formats_filter_lists'][$filter_format] = $filter_lists_titles;
  }
}

/**
 * Theme implementation.
 *
 * @see template_preprocess_text_formats_report_stats()
 */
function theme_text_formats_report_stats(&$variables) {
  $content = '';

  $content .= '<h2>' . t("Custom blocks using the formats") . '</h2>';
  $content .= (empty($variables['custom_blocks'])) ? '<p>' . t("None") . '</p>' : theme('table', text_formats_report_prepare_table($variables['custom_blocks']));

  $entities = (isset($variables['entities'])) ? $variables['entities'] : array();
  if (empty($entities)) {
    return $content;
  }

  $tabs_list = '';
  $tabs_content = '';
  // Used for ui.tabs navigation.
  $id = 0;
  foreach ($entities as $filter_format => $row) {
    $id++;
    $tabs_list .= '<li><a href="#tabs-' . $id . '">' . $filter_format . '</a></li>';
    $tabs_content .= '<div id="tabs-' . $id . '">';

    $tabs_content .= '<p><strong>' . t("Total entities using <em>@format</em>:", array('@format' => $filter_format)) . ' </strong><em>' . $entities[$filter_format]['total_content'] . '</em></p>';

    $tabs_content .= theme('item_list', array(
      'items' => $variables['filter_formats_filter_lists'][$filter_format],
      'title' => t('Filters enabled for <em>@format</em> (@count)', array('@format' => $filter_format, '@count' => count($variables['filter_formats_filter_lists'][$filter_format]))),
    )
    );
    $tabs_content .= '<p>' . l(t("Configure @format", array('@format' => $filter_format)), "admin/config/content/formats/$filter_format") . '</p>';

    $tabs_content .= '<h3>' . t("Bundles using <em>@format</em> (@count)", array('@format' => $filter_format, '@count' => count($entities[$filter_format]['bundles']))) . '</h3>';
    $tabs_content .= theme('table', text_formats_report_prepare_simple_table($entities[$filter_format]['bundles'], array('Bundle name', 'Entities')));

    $tabs_content .= '<h3>' . t("Fields using <em>@format</em> (@count)", array('@format' => $filter_format, '@count' => count($entities[$filter_format]['fields']))) . '</h3>';
    $tabs_content .= '<p><em>' . t("Click on the field names to see the content in them.") . '</em></p>';
    $tabs_content .= theme('table', text_formats_report_prepare_simple_table($entities[$filter_format]['fields'], array('Field name', 'Entities')));
    $tabs_content .= '</div>';
  }

  $content .= '<div id="filter-format-tabs">';
  $content .= '<div id="tab-list" class="item-list"><ul>' . $tabs_list . '</ul></div>';
  $content .= '<div id="tab-content">' . $tabs_content . '</div>';
  $content .= '</div>';

  return $content;
}
