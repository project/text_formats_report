<?php

/**
 * @file
 * Form and related functions for admin/reports/text-formats.
 */

module_load_include('inc', 'text_formats_report', 'inc/text_formats_report');

/**
 * Menu callback.
 *
 * Provides input formats report form.
 */
function text_formats_report_page_form($form, &$form_state) {

  $filter_format_options = text_formats_report_filter_format_options();

  $form['formats'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Select the text format(s) to analyze"),
    '#options' => $filter_format_options,
    '#required' => TRUE,
    '#default_value' => array(),
  );
  $form['submit'] = array(
    '#value' => t("View"),
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'text_formats_report_page_form_submit_ajax_handler',
      'effect' => 'fade',
      'wrapper' => 'js-format-stats',
    ),
  );
  $form['download'] = array(
    '#value' => t("Download CSV"),
    '#type' => 'submit',
  );

  $form['js-format-stats'] = array(
    '#markup' => '<div id="js-format-stats"></div>',
  );

  $form['#attached']['library'][] = array('system', 'ui.tabs');

  return $form;
}

/**
 * Form submit handler.
 */
function text_formats_report_page_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t("Download CSV")) {
    text_formats_report_page_form_download($form, $form_state);
  }
}

/**
 * Form 'View' submit ajax handler.
 */
function text_formats_report_page_form_submit_ajax_handler($form, &$form_state) {
  $content = '<div id="js-format-stats">';

  // If we have errors, return early and display them.
  if (form_get_errors()) {
    return array(
      '#type' => 'ajax',
      '#commands' => array(
        ajax_command_replace('#js-format-stats', $content . theme('status_messages') . '</div>'),
      ),
    );
  }

  $selected_formats = array();
  if (!empty($form_state['values']) && isset($form_state['values']['formats'])) {
    foreach ($form_state['values']['formats'] as $format) {
      if ($format) {
        $selected_formats[] = $format;
      }
    }
  }
  $selected_formats_filter_lists = text_formats_report_filter_format_filter_list($selected_formats);
  $custom_blocks = text_formats_report_get_custom_blocks($selected_formats);
  $entities = text_formats_report_fetch_stats($selected_formats);

  $content .= theme('text_formats_report_stats', array(
    'entities' => $entities,
    'custom_blocks' => $custom_blocks,
    'filter_formats_filter_lists' => $selected_formats_filter_lists,
  ));

  $content .= '</div>';

  $result = array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#js-format-stats', $content),
      ajax_command_invoke('#filter-format-tabs', 'tabs'),
    ),
  );
  return $result;
}

/**
 * Serves the data for the selected filter formats as a csv file and exits.
 */
function text_formats_report_page_form_download($form, &$form_state) {
  $selected_formats = array();
  if (!empty($form_state['values']) && isset($form_state['values']['formats'])) {
    foreach ($form_state['values']['formats'] as $format) {
      if ($format) {
        $selected_formats[] = $format;
      }
    }
  }

  // @todo include the custom blocks in the csv file.
  // $custom_blocks = text_formats_report_get_custom_blocks($selected_formats);
  $stats = text_formats_report_fetch_stats_for_csv($selected_formats);
  $file_name = "export-" . date("d-m-Y-H-i") . ".csv";

  drupal_add_http_header('Content-type', 'application/csv; charset=ISO-8859-1');
  drupal_add_http_header('Content-Disposition', "attachment; filename=$file_name");

  $output = fopen('php://output', 'w');
  fputcsv($output, array(
    t("Showing data for the filter formats: @formats", array('@formats' => implode(', ', $selected_formats))),
  ));
  fputcsv($output, array(''));

  foreach ($stats as $row) {
    fputcsv($output, $row);
  }

  drupal_exit();
}
