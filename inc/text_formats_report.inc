<?php

/**
 * @file
 * Helper functions for text_formats_report module.
 */

/**
 * Fetches the filter_format filter list for an array of filter formats.
 *
 * @param array $filter_formats
 *   Set of filter_formats ids.
 *
 * @return array
 *   Rows of filter_list_format() results for each filter_format provided.
 */
function text_formats_report_filter_format_filter_list(array $filter_formats = array()) {
  $filter_formats_info = array();
  foreach ($filter_formats as $filter_format) {
    $filter_formats_info[$filter_format] = filter_list_format($filter_format);
  }
  return $filter_formats_info;
}

/**
 * Gathers the stats fby filter format.
 *
 * @param array $filter_formats
 *   All if none passed in.
 * @param array $field_names
 *   All if none passed in.
 *
 * @return array
 *   (
 *     'filter_format' => array(
 *       'bundles',
 *       'fields',
 *       'fields_usage',
 *      ),
 *   )
 */
function text_formats_report_fetch_stats(array $filter_formats = array(), array $field_names = array()) {
  if (empty($field_names)) {
    $candiate_fields = text_formats_report_get_field_configs();
    foreach ($candiate_fields as $field) {
      $field_names[] = $field['field_name'];
    }
  }

  $stats = array();
  foreach ($filter_formats as $filter_format) {
    $stats[$filter_format] = array(
      'bundles' => array(),
      'fields' => array(),
      'total_content' => 0,
    );
  }

  foreach ($field_names as $field_name) {
    $records = text_formats_report_get_field_usage($field_name, $filter_formats);

    foreach ($records as $record) {
      foreach ($filter_formats as $filter_format) {
        if ($record->format == $filter_format) {
          $stats[$filter_format]['total_content']++;
          $field_label = l($field_name, 'admin/reports/text-formats/field/' . $field_name, array('query' => array('formats' => $filter_format)));
          $stats[$filter_format]['bundles'][] = $record->bundle;
          $stats[$filter_format]['fields'][] = $field_label;
        }
      }
    }
  }

  // Calculate some stats per filter_format.
  foreach ($stats as $filter_format => $row) {
    $bundles = array_count_values($stats[$filter_format]['bundles']);
    $stats[$filter_format]['bundles'] = $bundles;

    $fields = array_count_values($stats[$filter_format]['fields']);
    $stats[$filter_format]['fields'] = $fields;
  }

  return $stats;
}

/**
 * Gathers the stats by filter format.
 *
 * @param array $filter_formats
 *   All if none passed in.
 * @param array $field_names
 *   All if none passed in.
 *
 * @return array
 *   The gathered statistics for the filter formats and fields requested.
 */
function text_formats_report_fetch_stats_for_csv(array $filter_formats = array(), array $field_names = array()) {
  if (empty($field_names)) {
    $candiate_fields = text_formats_report_get_field_configs();
    foreach ($candiate_fields as $field) {
      $field_names[] = $field['field_name'];
    }
  }

  $stats = array();
  $stats[] = array(
    t('Entity Id'),
    t('Entity type'),
    t('Bundle'), t('Field name'),
    t('Text format'),
    t('Path'),
    t('Content'),
  );

  foreach ($field_names as $field_name) {
    $records = text_formats_report_get_field_usage($field_name, $filter_formats);
    foreach ($records as $record) {
      $path = '';
      if ($record->entity_type == 'node') {
        $path = '/node/' . $record->entity_id;
      }
      $stats[] = array(
        $record->entity_id,
        $record->entity_type,
        $record->bundle,
        $field_name,
        $record->format,
        $path,
        $record->value,
      );
    }
  }
  return $stats;
}

/**
 * Helper function to get available input formats as form options.
 *
 * @return array
 *   Key format value namefilter_format.format => filter_format.name
 */
function text_formats_report_filter_format_options() {
  // Get list of all input formats.
  $query = db_query('SELECT format, name FROM {filter_format}');
  $records = $query->fetchAll();
  foreach ($records as $record) {
    $options[$record->format] = $record->name;
  }
  return $options;
}

/**
 * Helper function to retrieve the custom blocks using specified filter formats.
 *
 * @param array $filter_formats
 *   The filter_format.format to check for.
 *
 * @return array
 *   (
 *     array('bid', 'info', 'format'),
 *   )
 */
function text_formats_report_get_custom_blocks(array $filter_formats) {
  $custom_blocks = array();
  $query = db_select('block_custom', 'b')
    ->condition('b.format', $filter_formats)
    ->fields('b', array('bid', 'info', 'format'))
    ->execute();
  if ($query->rowCount() > 0) {
    while ($row = $query->fetchAssoc()) {
      $custom_blocks[] = $row;
    }
  }
  return $custom_blocks;
}

/**
 * Helper function to retrieve the fields using filter formats.
 *
 * Gets the fields of type 'text', 'text_with_summary' or'text_long'
 *
 * @return array
 *   Rows from the field_config table including field_name, id and type.
 */
function text_formats_report_get_field_configs() {
  $field_names = array();
  $query = db_select('field_config', 'fc')
    ->condition('fc.type', array('text', 'text_with_summary', 'text_long'))
    ->fields('fc', array('field_name', 'id', 'type'))
    ->execute();
  if ($query->rowCount() > 0) {
    while ($row = $query->fetchAssoc()) {
      $field_names[] = $row;
    }
  }
  return $field_names;
}

/**
 * Retrieves entries from the fields data table.
 *
 * @param string $field_name
 *   The field to get the data for.
 * @param array $filter_formats
 *   The filter formats to filter by. All if none passed in.
 */
function text_formats_report_get_field_usage($field_name, array $filter_formats = array()) {
  $filter_formats = empty($filter_formats) ? array_keys(text_formats_report_filter_format_options()) : $filter_formats;

  $format_column = $field_name . '_format';
  $value_column = $field_name . '_value';

  $filter = "";
  if (!empty($filter_formats)) {
    $formats_query_string = '(';
    foreach ($filter_formats as $format) {
      $formats_query_string .= "'$format',";
    }
    $formats_query_string = rtrim($formats_query_string, ",");
    $formats_query_string .= ')';
    $filter = " WHERE $format_column IN $formats_query_string";
  }

  $result = db_query("SELECT entity_id,entity_type, bundle, $format_column as format, $value_column as value FROM {field_data_$field_name}" . $filter);
  return $result->fetchAll();
}

/**
 * Helper function that prepares array for theme_table.
 *
 * @param array $data
 *   Rows composed of header-label => row value.
 * @param array $header
 *   Optional header for the table.
 *
 * @return string
 *   Output from theme_table.
 */
function text_formats_report_prepare_table(array $data, array $header = array()) {
  // Construct the table headers array if necessary.
  if (empty($header)) {
    foreach ($data as $row) {
      foreach ($row as $title => $content) {
        $header[] = $title;
      }
      break;
    }
  }

  // Construct the table rows data array.
  $row_id = 0;
  $rows = array();
  foreach ($data as $row) {
    foreach ($row as $title => $content) {
      if ($title == 'value') {
        $content = htmlentities($content);
      }
      $rows[$row_id][] = $content;
    }
    $row_id++;
  }

  return array(
    'header' => $header,
    'rows' => $rows,
  );
}

/**
 * Helper function that prepares a label => value array for theme_table.
 *
 * @param array $data
 *   The data to prepare to be presented as a simple table.
 * @param array $header
 *   Optional header for the table.
 *
 * @return string
 *   Output from theme_table.
 */
function text_formats_report_prepare_simple_table(array $data, array $header = array()) {
  $data = (array) $data;
  $rows = array();

  foreach ($data as $label => $value) {
    $rows[] = array('<strong>' . $label . '</strong>', $value);
  }

  return array(
    'header' => $header,
    'rows' => $rows,
  );
}
