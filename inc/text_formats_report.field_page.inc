<?php

/**
 * @file
 * Callback for admin/reports/text-formats/field/%.
 */

module_load_include('inc', 'text_formats_report', 'inc/text_formats_report');

/**
 * Menu callback.
 */
function text_formats_report_field_page($field_name) {
  if (!isset($_GET['formats']) || empty($_GET['formats'])) {
    return drupal_not_found();
  }

  $field_name = check_plain($field_name);
  $filter_formats = $_GET['formats'];

  // In case only one format passed via url.
  if (is_string($filter_formats)) {
    $filter_formats = array($filter_formats);
  }
  foreach ($filter_formats as $key => $format) {
    $filter_formats[$key] = check_plain($format);
  }

  $records = text_formats_report_get_field_usage($field_name, $filter_formats);

  return theme('text_formats_report_field_stats', array(
    'stats' => $records,
    'filter_formats' => $filter_formats,
    'field_name' => $field_name,
  ));
}
